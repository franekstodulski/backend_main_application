const express = require('express');

const studentRouter = express.Router();


const {
  getStudentDetails,
  addNewStudent,
} = require('../controllers/student');

studentRouter.route('/students/details/:id').get(getStudentDetails);
studentRouter.route('/students/add').post(addNewStudent)

module.exports = studentRouter;
