const express = require('express');

const trainerRouterPublic = express.Router();
const trainerRouterSecured = express.Router();


const {
  registerTrainer,
  loginTrainer,
  getTrainerData,
} = require('../controllers/trainer');


// Public
trainerRouterPublic.route('/trainer/register').post(registerTrainer);
trainerRouterPublic.route('/trainer/login').post(loginTrainer);


// Secured
trainerRouterSecured.route('/trainer/:id').get(getTrainerData);


module.exports = {
  trainerRouterPublic,
  trainerRouterSecured,
};
