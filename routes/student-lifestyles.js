const express = require('express');

const studentLifestyleRouter = express.Router();

const {
  addNewLifeStyle,
} = require('../controllers/student-lifestyles');

studentLifestyleRouter.route('/lifestyles/add').post(addNewLifeStyle);

module.exports = studentLifestyleRouter;
