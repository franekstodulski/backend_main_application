const express = require('express');

const foodRouter = express.Router();

const {
  addNewFoodProduct,
  getAllProducts,
} = require('../controllers/food-product');

const {
  addNewFoodCategory,
} = require('../controllers/food-category');


foodRouter.route('/food/product/add').post(addNewFoodProduct);
foodRouter.route('/food/product/all').get(getAllProducts);
foodRouter.route('/food/category/add').post(addNewFoodCategory);


module.exports = foodRouter;
