const express = require('express');

const rootAdminRouter = express.Router();

const {
  getRootUsers,
  getRootUser,
  deleteRootUser,
  addNewRootUser,
} = require('../controllers/root_admin');


rootAdminRouter.route('/root_admin/all').get(getRootUsers);
rootAdminRouter.route('/root_admin/details/:id').get(getRootUser);
rootAdminRouter.route('/root_admin/delete/:id').delete(deleteRootUser);
rootAdminRouter.route('/root_admin/add').post(addNewRootUser);

module.exports = rootAdminRouter;
