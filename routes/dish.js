const express = require('express');

const dishRouter = express.Router();


const {
  getAllDishes,
  getDishDetails,
  addDish,
} = require('../controllers/dish');

dishRouter.route('/dish/all').get(getAllDishes);
dishRouter.route('/dish/details/:id').get(getDishDetails);
dishRouter.route('/dish/add').post(addDish);


module.exports = dishRouter;
