module.exports = {
  up: async (queryInterface, Sequelize) => {
    const createTable = await queryInterface.createTable(
      'food_product_reporteds', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
      }, {
        charset: 'utf8',
      },
    );

    const food_product_reported__food_products = await queryInterface.addColumn(
      'food_product_reporteds',
      'foodProductId', {
        type: Sequelize.INTEGER,
        references: {
          model: 'food_products',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
    return await Promise.all([createTable, food_product_reported__food_products]);
  },
  down: (queryInterface, Sequelize) => queryInterface.dropTable('food_product_reporteds'),
};
