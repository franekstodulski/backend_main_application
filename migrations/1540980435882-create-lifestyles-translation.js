module.exports = {
  up: async (queryInterface, Sequelize) => {
    const createTable = await queryInterface.createTable('lifestyles_translations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      name: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    }, {
      charset: 'utf8',
    });

    const lifestyles_translation__lifestyles = await queryInterface.addColumn(
      'lifestyles_translations',
      'typeId', {
        type: Sequelize.INTEGER,
        references: {
          model: 'food_products',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );

    const lifestyles_translation__languages = await queryInterface.addColumn(
      'lifestyles_translations',
      'languageId', {
        type: Sequelize.INTEGER,
        references: {
          model: 'languages',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );

    return await Promise.all([createTable, lifestyles_translation__lifestyles, lifestyles_translation__languages]);
  },
  down: (queryInterface, Sequelize) => queryInterface.dropTable('lifestyles_translations'),
};
