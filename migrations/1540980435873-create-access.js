module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable(
    'accesses', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      level: {
        type: Sequelize.INTEGER,
      },
      name: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    }, {
      charset: 'utf8',
    },
  ),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('accesses'),
};
