module.exports = {
  up: async (queryInterface, Sequelize) => {
    const createTable = await queryInterface.createTable(
      'food_products', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        originalName: {
          type: Sequelize.STRING,
        },
        weight: {
          type: Sequelize.INTEGER,
        },
        kcal: {
          type: Sequelize.INTEGER,
        },
        protein: {
          type: Sequelize.INTEGER,
        },
        carbs: {
          type: Sequelize.INTEGER,
        },
        simpleSugars: {
          type: Sequelize.INTEGER,
        },
        roughage: {
          type: Sequelize.INTEGER,
        },
        salt: {
          type: Sequelize.INTEGER,
        },
        fat: {
          type: Sequelize.INTEGER,
        },
        activated: {
          type: Sequelize.BOOLEAN,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
      }, {
        charset: 'utf8',
      },
    );

    const food_products__food_categories = await queryInterface.addColumn(
      'food_products',
      'foodCategoryId', {
        type: Sequelize.INTEGER,
        references: {
          model: 'food_categories', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );

    return await Promise.all([createTable, food_products__food_categories]);
  },
  down: (queryInterface, Sequelize) => queryInterface.dropTable('food'),
};
