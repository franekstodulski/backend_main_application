module.exports = {
  up: async (queryInterface, Sequelize) => {
    const createTable = await queryInterface.createTable('students', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      firstName: {
        type: Sequelize.STRING,
      },
      lastName: {
        type: Sequelize.STRING,
      },
      email: {
        type: Sequelize.STRING,
      },
      location: {
        type: Sequelize.STRING,
      },
      weight: {
        type: Sequelize.INTEGER,
      },
      height: {
        type: Sequelize.INTEGER,
      },
      birthday: {
        type: Sequelize.STRING,
      },
      waist: {
        type: Sequelize.INTEGER,
      },
      shoulders: {
        type: Sequelize.INTEGER,
      },
      chest: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    }, {
      charset: 'utf8',
    });


    const student__trainer = await queryInterface.addColumn(
      'students',
      'trainerId', {
        type: Sequelize.INTEGER,
        references: {
          model: 'trainers',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );


    return await Promise.all([createTable, student__trainer]);
  },
  down: (queryInterface, Sequelize) => queryInterface.dropTable('students'),
};
