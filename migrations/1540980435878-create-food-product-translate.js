module.exports = {
  up: async (queryInterface, Sequelize) => {
    const createTable = await queryInterface.createTable(
      'food_product_translates', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        name: {
          type: Sequelize.STRING,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
      }, {
        charset: 'utf8',
      },
    );

    const food_products_translate__food_products = await queryInterface.addColumn(
      'food_product_translates',
      'foodProductId', {
        type: Sequelize.INTEGER,
        references: {
          model: 'food_products',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );

    const food_products_translate__languages = await queryInterface.addColumn(
      'food_product_translates',
      'languageId', {
        type: Sequelize.INTEGER,
        references: {
          model: 'languages',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );

    return await Promise.all([
      createTable,
      food_products_translate__languages,
      food_products_translate__food_products,
    ]);
  },
  down: (queryInterface, Sequelize) => queryInterface.dropTable('food_product_translates'),
};
