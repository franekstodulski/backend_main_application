module.exports = {
  up: async (queryInterface, Sequelize) => {
    const createTable = await queryInterface.createTable('comments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      content: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    }, {
      charset: 'utf8',
    });

    const comments__trainers = await queryInterface.addColumn(
      'comments',
      'authorId', {
        type: Sequelize.INTEGER,
        references: {
          model: 'trainers',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );

    const comments__dish = await queryInterface.addColumn(
      'comments',
      'dishId', {
        type: Sequelize.INTEGER,
        references: {
          model: 'dishes',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
    return await Promise.all([createTable, comments__trainers, comments__dish]);
  },
  down: (queryInterface, Sequelize) => queryInterface.dropTable('comments'),
};
