module.exports = {
  up: async (queryInterface, Sequelize) => {
    const createTable = await queryInterface.createTable('dish_products', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      weight: {
        type: Sequelize.INTEGER,
      },
      kcal: {
        type: Sequelize.INTEGER,
      },
      protein: {
        type: Sequelize.INTEGER,
      },
      carbs: {
        type: Sequelize.INTEGER,
      },
      simpleSugars: {
        type: Sequelize.INTEGER,
      },
      roughage: {
        type: Sequelize.INTEGER,
      },
      salt: {
        type: Sequelize.INTEGER,
      },
      fat: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    }, {
      charset: 'utf8',
    });


    const dish_product__dish = await queryInterface.addColumn(
      'dish_products',
      'dishId', {
        type: Sequelize.INTEGER,
        references: {
          model: 'dishes',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );

    return await Promise.all([createTable, dish_product__dish]);
  },
  down: (queryInterface, Sequelize) => queryInterface.dropTable('dish_products'),
};
