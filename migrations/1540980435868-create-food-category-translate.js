module.exports = {
  up: async (queryInterface, Sequelize) => {
    const createTable = await queryInterface.createTable(
      'food_category_translates', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        name: {
          type: Sequelize.STRING,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
      }, {
        charset: 'utf8',
      },
    );

    const food_category__food_category_translate = await queryInterface.addColumn(
      'food_category_translates',
      'foodCategoryId', {
        type: Sequelize.INTEGER,
        references: {
          model: 'food_categories',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );

    const food_category_translate__languages = await queryInterface.addColumn(
      'food_category_translates',
      'languageId', {
        type: Sequelize.INTEGER,
        references: {
          model: 'languages',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );

    return await Promise.all([
      createTable,
      food_category__food_category_translate,
      food_category_translate__languages,
    ]);
  },
  down: (queryInterface, Sequelize) => queryInterface.dropTable('food_category_translates'),
};
