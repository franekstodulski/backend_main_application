module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('food_categories', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    activated: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  }, {
    charset: 'utf8',
  }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('food_categories'),
};
