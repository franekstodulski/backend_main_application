module.exports = {
  up: async (queryInterface, Sequelize) => {
    const createTable = await queryInterface.createTable(
      'assets_trainers', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        filePath: {
          type: Sequelize.STRING,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
      }, {
        charset: 'utf8',
      },
    );

    const assets_trainer__assets_types = await queryInterface.addColumn(
      'assets_trainers',
      'typeId', {
        type: Sequelize.INTEGER,
        references: {
          model: 'assets_types',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );
    const assets_trainer__trainer = await queryInterface.addColumn('assets_trainers', 'trainerId', {
      type: Sequelize.INTEGER,
      references: {
        model: 'trainers',
        key: 'id',
      },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
    });

    return await Promise.all([createTable, assets_trainer__assets_types, assets_trainer__trainer]);
  },
  down: (queryInterface, Sequelize) => queryInterface.dropTable('assets_trainers'),
};
