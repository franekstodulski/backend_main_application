module.exports = {
  up: async (queryInterface, Sequelize) => {
    const createTable = await queryInterface.createTable(
      'trainers', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        email: {
          type: Sequelize.STRING,
        },
        firstName: {
          type: Sequelize.STRING,
        },
        lastName: {
          type: Sequelize.STRING,
        },
        birthday: {
          type: Sequelize.STRING,
        },
        location: {
          type: Sequelize.STRING,
        },
        activated: {
          type: Sequelize.BOOLEAN,
        },
        recurring_payments: {
          type: Sequelize.BOOLEAN,
        },
        referer_url: {
          type: Sequelize.STRING,
        },
        points: {
          type: Sequelize.INTEGER,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
      }, {
        charset: 'utf8',
      },
    );

    const trainer__access = await queryInterface.addColumn('trainers', 'accessLevelId', {
      type: Sequelize.INTEGER,
      references: {
        model: 'accesses',
        key: 'id',
      },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
    });

    return await Promise.all([createTable, trainer__access]);
  },
  down: (queryInterface, Sequelize) => queryInterface.dropTable('trainers'),
};
