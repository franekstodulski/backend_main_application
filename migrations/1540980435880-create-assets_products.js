module.exports = {
  up: async (queryInterface, Sequelize) => {
    const createTable = await queryInterface.createTable(
      'assets_products', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        filePath: {
          type: Sequelize.STRING,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
      }, {
        charset: 'utf8',
      },
    );

    const assets_products__assets_types = await queryInterface.addColumn(
      'assets_products',
      'typeId', {
        type: Sequelize.INTEGER,
        references: {
          model: 'assets_types',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );

    const assets_products__food_product = await queryInterface.addColumn(
      'assets_products',
      'foodProductId', {
        type: Sequelize.INTEGER,
        references: {
          model: 'food_products',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      },
    );

    return await Promise.all([
      createTable,
      assets_products__assets_types,
      assets_products__food_product,
    ]);
  },
  down: (queryInterface, Sequelize) => queryInterface.dropTable('assets_products'),
};
