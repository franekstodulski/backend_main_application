module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('dish_categories', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    name: {
      type: Sequelize.STRING,
    },
    activated: {
      type: Sequelize.BOOLEAN,
    },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  }, {
    charset: 'utf8',
  }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('dish_categories'),
};
