module.exports = {
  up: async (queryInterface, Sequelize) => {
    const createTable = await queryInterface.createTable(
      'dishes', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        name: {
          type: Sequelize.STRING,
        },
        describtion: {
          type: Sequelize.STRING,
        },
        likes: {
          type: Sequelize.INTEGER,
        },
        dislikes: {
          type: Sequelize.INTEGER,
        },
        activated: {
          type: Sequelize.BOOLEAN,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
      }, {
        charset: 'utf8',
      },
    );

    const dish__trainer = await queryInterface.addColumn('dishes', 'authorId', {
      type: Sequelize.INTEGER,
      references: {
        model: 'dishes',
        key: 'id',
      },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
    });
    return await Promise.all([createTable, dish__trainer]);
  },
  down: (queryInterface, Sequelize) => queryInterface.dropTable('dishes'),
};
