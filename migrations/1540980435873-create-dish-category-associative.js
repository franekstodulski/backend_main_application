module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable(
    'dish_category_associatives', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      dishID: {
        type: Sequelize.INTEGER,
      },
      dishCategoryID: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    }, {
      charset: 'utf8',
    },
  ),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('dish_category_associatives'),
};
