module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable('assets_types', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    type: {
      type: Sequelize.STRING,
    },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE,
    },
  }, {
    charset: 'utf8',
  }),
  down: (queryInterface, Sequelize) => queryInterface.dropTable('assets_types'),
};
