const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const fileupload = require('express-fileupload');
const path = require('path');
const fs = require('fs');
const rateLimit = require('express-rate-limit');
const helmet = require('helmet');
const morgan = require('morgan');
// Create secret key
process.env.RSA_PRIVATE_KEY = 'asdsadsadsadsadas21ead1easdfsada';

// Initialize express application
const app = express();
// Disable X-powered-By
app.disable('x-powered-by');

// Use Route for 3 different paths
const publicRoutes = express.Router();
const securesRoutes = express.Router();
const postmanRoutes = express.Router();

// Rate Limiter
const requestLimiter = rateLimit({
  windowMs: 16 * 60 * 1000,
  max: 100,
});

// CORS options
const corsOptions = {
  credentials: true,
  origin: 'http://localhost:4200',
};

// Morgan, loggin
const accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {
  flags: 'a',
});

publicRoutes.use(morgan('combined', {
  stream: accessLogStream,
}));


// Create prefix for all API routes
app.use('/api/v1/public', publicRoutes);
app.use('/api/v1/secured', securesRoutes);
app.use('/postman', postmanRoutes);
app.use('/assets', express.static(path.join(__dirname, 'assets')));

// Use bodyParser for public routes
publicRoutes.use(cors(corsOptions));
publicRoutes.use(requestLimiter);
publicRoutes.use(
  bodyParser.json({
    limit: '50mb',
  }),
);
publicRoutes.use(
  bodyParser.urlencoded({
    limit: '50mb',
    extended: true,
  }),
);

const {
  jwtValidate,
} = require('./middlewares/authenticate');

securesRoutes.use(requestLimiter);
securesRoutes.use(cors(corsOptions));
securesRoutes.use(jwtValidate);
securesRoutes.use(fileupload());
securesRoutes.use(helmet());

// Use bodyParser for secured routes
securesRoutes.use(
  bodyParser.json({
    limit: '50mb',
  }),
);
securesRoutes.use(
  bodyParser.urlencoded({
    limit: '50mb',
    extended: true,
  }),
);

// API ENDPOINTS


// Root ADMIN API
const rootAdminRouter = require('./routes/root-admin');
const {
  loginRootUser,
} = require('./controllers/root_admin');

securesRoutes.use(rootAdminRouter);
publicRoutes.post('/root_admin/login', loginRootUser);

// Add dish product
const dishRouter = require('./routes/dish');

securesRoutes.use(dishRouter);

// Students
const studentRouter = require('./routes/stundet');

securesRoutes.use(studentRouter);

// FOOD Products
const foodRouter = require('./routes/food');

securesRoutes.use(foodRouter);

// LifeStyles
const studentLifestyleRouter = require('./routes/student-lifestyles');

securesRoutes.use(studentLifestyleRouter);


// Trainer API
const {
  trainerRouterPublic,
  trainerRouterSecured,
} = require('./routes/trainer');

publicRoutes.use(trainerRouterPublic);
securesRoutes.use(trainerRouterSecured);

// Define port for serverprAPP
const PORT = process.env.PORT || 8081;

app.get('/test', (req, res) => {
  res.send('Works!');
});

app.listen(PORT, () => {
  console.log(`Node Express server listening on http://localhost:${PORT}`);
});
