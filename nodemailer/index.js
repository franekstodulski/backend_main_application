const nodeMailerConfig = {
  host: 'mail16.mydevil.net',
  port: 465,
  secureConnection: true,
  auth: {
    user: 'f.stodulski@majesticdev.co',
    pass: 'Pdt2k4o9',
  },
};

const nodeMailerTemplate = (user, userLink, additionalMessage) => `
    <h1> Hello <strong>${user}</strong></h1>,
    
    <p>Click below to activate your account</p>
    <p> <a href="${userLink}" target="_blank">Click me!</a></p>

    <p>${additionalMessage}</p>
    `;


const nodeMailerOptions = {
  from: 'f.stodulski@majesticdev.co', // sender address
  to: '', // list of receivers
  subject: '', // Subject line
  html: '<p> hello world</p>', // plain text body
};


module.exports = {
  nodeMailerConfig,
  nodeMailerOptions,
  nodeMailerTemplate,
};
