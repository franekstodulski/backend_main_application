const ErrorHandler = require('./error-handler');

class ResponseHandler {
  constructor(req, res, responseObject) {
    this.request = req;
    this.response = res;
    this.responseObject = responseObject;
  }

  handleResponse(responseObj) {
    if (this.responseObject) {
      this.response.status(200).send(responseObj);
    } else {
      this.response.status(404).send({
        message: 'Not found',
      });
    }
  }
}

module.exports = ResponseHandler;
