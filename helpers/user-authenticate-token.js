const jwt = require('jsonwebtoken');


/**
 * Authenticate user
 * @param {number} id Id of user
 * @returns token
 */
async function userAuthenticate(id) {
  const token = jwt.sign({
    sessionID: id,
  }, process.env.RSA_PRIVATE_KEY, {
    expiresIn: '1h',
  });

  return token;
}

async function userDecodeToken(token) {
  const user = jwt.decode(token);

  return user;
}

module.exports = {
  userAuthenticate,
  userDecodeToken,
};
