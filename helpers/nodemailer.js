const nodemailer = require('nodemailer');

const {
  nodeMailerConfig,
  nodeMailerOptions,
  nodeMailerTemplate,
} = require('../nodemailer');

const transporter = nodemailer.createTransport(nodeMailerConfig);


async function sendActivationLink({
  userName,
  userEmail,
  userLink,
  subject,
  additionalMessage,
}) {
  const myNodeMailerOptions = {
    ...nodeMailerOptions,
    to: userEmail,
    subject,
    html: nodeMailerTemplate(userName, userLink, additionalMessage),
  };

  return new Promise((resolve, reject) => {
    const sent = transporter.sendMail(myNodeMailerOptions, (err, info) => {
      if (!err) {
        resolve(true);
      } else {
        reject(false);
      }
    });
  });
}


module.exports = {
  sendActivationLink,
};
