const models = require('../models');

/**
 *
 */
class QueryHandler {
  /**
     *
     * @param {*} options
     */
  constructor({
    model = null,
    queryType = '',
    whereQuery = {},
    attributes = [],
    include = [],
  }) {
    this.options = {
      model,
      queryType,
      whereQuery,
      attributes,
      include,
    };
  }


  async query() {
    this.options.whereQuery = {} ? null : this.options.whereQuery;
    this.options.include = [] ? null : this.options.include;
    this.options.attributes = [] ? null : this.options.attributes;

    const {
      where,
      include,
      attributes,
    } = this.options;


    return models[this.options.model][this.options.queryType]({
      where,
      include,
      attributes,
    })
      .then(result => result)
      .catch(resultError => resultError);
  }


  showOptions() {
    return this.options;
  }
}


module.exports = QueryHandler;
