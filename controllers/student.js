const models = require('../models');

async function getStudentDetails(req, res) {}

async function addNewStudent(req, res) {
  const {
    body
  } = req;

  if (body) {
    const {
      firstName,
      lastName,
      email,
      birthday,
      location,
      weight,
      height,
      waist,
      shoulders,
      chest,
      trainerId,
    } = body;

    const createNewStudent = await models.student
      .create({
        firstName,
        lastName,
        email,
        birthday,
        location,
        weight,
        height,
        waist,
        shoulders,
        chest,
        trainerId,
      })
      .then(createdStudent => createdStudent)
      .catch(error => error);

    if (createNewStudent) {
      res.status(200).send(createNewStudent);
    }
  }
}

module.exports = {
  getStudentDetails,
  addNewStudent,
};
