const axios = require('axios');
// const {
//   StudentsLifeStyle,
// } = require('../models');


const {
  yandex,
} = require('../env');

async function addNewLifeStyle(req, res) {
  const {
    body,
    headers,
  } = req;


  const {
    type,
  } = body;

  const {
    lang,
  } = headers;


  if (type && lang) {
    const translateReq = await axios({
      method: 'get',
      url: `https://translate.yandex.net/api/v1.5/tr.json/translate?key=${yandex.translate_api_key}&text=${encodeURI(type)}&lang=${'pl'}-${'en'}`,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    }).then(translateRes => translateRes.data.text[0]).catch(error => console.log(error));
  }
}


module.exports = {
  addNewLifeStyle,
};
