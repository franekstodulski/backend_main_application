// const Sequelize = require('sequelize');

// Includes
const {
  food_category,
  food_category_translate,
} = require('../models');

async function addNewFoodCategory(req, res) {
  const {
    body,
  } = req;

  if (body) {
    const {
      name,
      lang,
    } = body;

    const createNewCategoryEntity = await food_category
      .create()
      .then(createdObject => createdObject.id)
      .catch(createdObjectError => res.status(500).send(createdObjectError));
  }
}

module.exports = {
  addNewFoodCategory,
};
