const {
  comment,
  dish,
  dish_product,
  dish_category_associative,
  food_product_details,
  trainer,
  languages,
} = require('../models');


/**
 * Add new dish to DB
 * @param {*} req HTTP Request
 * @param {*} res HTTP Response
 */
async function addDish(req, res) {
  const {
    body,
  } = req;


  if (body) {
    const {
      name,
      description,
      trainerId,
      languageId,
      categories,
      products,
    } = body;

    if (name && description && categories) {
      const addDishPromise = await dish
        .create({
          name,
          description,
          trainerId,
          languageId,
        })
        .then(response => response)
        .catch(error => error);

      // If dish has been created, create associative element in table
      const createAssociativeObject = await categories
        .map(async (category) => {
          const insert = await dish_category_associative
            .create({
              dishCategoryId: category,
              dishId: addDishPromise.id,
            })
            .then(createdAssociative => createdAssociative)
            .catch(error => error);
        });

      const insertProductsForDish = await products
        .map(async (product) => {
          const {
            id,
            weight,
            kcal,
            protein,
            carbs,
            simpleSugars,
            roughage,
            salt,
            fat,
          } = product;

          const insertNewDishProduct = await dish_product
            .create({
              weight,
              kcal,
              protein,
              carbs,
              simpleSugars,
              roughage,
              salt,
              fat,
              dishId: addDishPromise.id,
              foodProductsDetailId: id,
            })
            .then(createdDishProduct => createdDishProduct)
            .catch(createdDishProductError => createdDishProductError);
        });

      // Gater all promises
      const gatherAllPromises = await Promise
        .all(createAssociativeObject)
        .then(results => results)
        .catch(error => error);


      if (gatherAllPromises) {
        res.status(200).send({
          message: 'Created',
        });
      } else {
        res.status(200).send({
          message: 'Cannot create',
        });
      }
    }
  }
}

/**
 * Get all dishes from DB
 * @param {*} req HTTP Request
 * @param {*} res HTTP Response
 */
async function getAllDishes(req, res) {
  const allDishes = await dish
    .findAll({
      limit: 100,
      include: [{
        model: trainer,
        attributes: ['id', 'firstName', 'lastName'],
      }],

    })
    .then(response => response)
    .catch(error => error);

  if (allDishes) {
    res.status(200).send(allDishes);
  } else {
    res.status(404).send({
      error: 'Dishes not found',
    });
  }
}


const trainerAttributes = ['firstName', 'lastName'];
/**
 * Get details of one dish
 * @param {*} req HTTP Request
 * @param {*} res HTTP Response
 */
async function getDishDetails(req, res) {
  const {
    params,
  } = req;

  if (params) {
    const {
      id,
    } = params;

    if (id) {
      const dishDetails = await dish
        .findOne({
          where: {
            id,
          },
          include: [{
              model: comment,
              include: [{
                model: trainer,
                attributes: trainerAttributes,
              }],
            },
            {
              model: trainer,
              attributes: trainerAttributes,
            },
          ],
        })
        .then(response => response)
        .catch(error => error);

      const dishProducts = await dish_product
        .findAll({
          where: {
            dishId: id,
          },
        })
        .then(foundDishProducts => foundDishProducts)
        .catch(foundDishProductsError => foundDishProductsError);


      if (dishDetails && dishProducts) {
        const combine = {
          dishDetails,
          foodProductsList: [...dishProducts],
        };
        res.status(200).send(
          combine,
        );
      } else {
        res.status(404).send({
          error: 'Dish not found',
        });
      }
    }
  }
}

module.exports = {
  getAllDishes,
  getDishDetails,
  addDish,
};
