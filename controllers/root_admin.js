const {
  root_admin,
} = require('../models');

const {
  userAuthenticate
} = require('../helpers/user-authenticate-token');


const attributes = [
  'name',
  'id',
  'email',
];

async function loginRootUser(req, res) {
  const {
    body,
  } = req;

  if (body) {
    const {
      password,
      email,
    } = body;

    if (password && email) {
      const checkIfUserExist = await root_admin.find({
        where: {
          password,
          email,
        },
        attributes,
      }).then((rootUser) => {
        if (rootUser) {
          const token = userAuthenticate(rootUser);

          return {
            rootUser,
            token,
          };
        }
        return {
          error: 'User not found',
        };
      }).catch(error => error);
      res.status(200).send(checkIfUserExist);
    }
  }
}


async function getRootUsers(req, res) {
  const allRootUsers = await root_admin.findAll({
    attributes,
  }).then(response => response).catch(err => err);

  if (allRootUsers.length > 0) {
    res.status(200).send(allRootUsers);
  } else {
    res.status(404).send({
      error: 'Users not found',
    });
  }
}


async function deleteRootUser(req, res) {
  const {
    params,
  } = req;


  const destroyUser = await root_admin.destroy({
    where: {
      id: params.id,
    },
  }).then(response => response).catch(error => error);


  getRootUsers(req, res);
}


async function getRootUser(req, res) {
  const {
    params,
  } = req;

  if (params) {
    const {
      id,
    } = params;


    const rootUserDetails = await root_admin.findOne({
      where: {
        id,
      },
      attributes,
    }).then(response => response).catch(error => error);

    if (rootUserDetails) {
      res.status(200).send(rootUserDetails);
    }
  }
}


async function addNewRootUser(req, res) {
  const {
    body,
  } = req;

  if (body) {
    const {
      name,
      password,
      email,
    } = body;


    const createUser = await root_admin.findOrCreate({
        where: {
          email,
        },
        defaults: {
          name,
          password,
          email,
        },
      })
      .spread((object, created) => {
        if (created) {
          return object;
        }
        return {
          error: 'User exist',
        };
      })
      .then(response => response).catch(error => error);


    if (createUser) {
      res.status(200).send(createUser);
    }
  }
}
module.exports = {
  loginRootUser,
  getRootUsers,
  getRootUser,
  deleteRootUser,
  addNewRootUser,
};
