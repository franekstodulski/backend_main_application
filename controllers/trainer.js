const models = require('../models');
const {
  sendActivationLink,
} = require('../helpers/nodemailer');

const {
  userAuthenticate,
} = require('../helpers/user-authenticate-token');


const QueryHandler = require('../helpers/query-handler');
const ResponseHandler = require('../helpers/response-handler');

async function registerTrainer(req, res) {
  const {
    body,
  } = req;

  if (body) {
    const {
      firstName,
      lastName,
      email,
    } = body;

    const registerTrainerPromise = await models.trainer
      .findOrCreate({
        where: {
          email,
          firstName,
          lastName,
        },
        defaults: {
          firstName,
          lastName,
        },
      })
      .spread((trainer, created) => ({
        trainer,
        created,
      }))
      .then(({
        trainer,
        created,
      }) => ({
        trainer,
        created,
      }))
      .catch(error => error);
    // If its new one
    if (registerTrainerPromise.created) {
      const sendMail = await sendActivationLink({
        userName: firstName,
        userEmail: email,
        userLink: 'asdasdasd',
        subject: 'Activation',
        additionalMessage: 'Welcome',
      });

      if (sendMail) {
        res.status(200).send({
          message: 'success',
        });
      } else {
        res.status(200).send({
          message: 'Problem with server, try again later',
        });
      }
    } else {
      // If trainer is in our database
      res.status(200).send({
        message: 'User already exist',
      });
    }
  }
}

async function loginTrainer(req, res) {
  const {
    body,
  } = req;

  if (body) {
    const {
      firstName,
      lastName,
      email,
    } = body;

    if (firstName && lastName && email) {
      const trainer = new QueryHandler({
        req,
        res,
        model: 'trainer',
        queryType: 'findOne',
        whereQuery: {
          firstName,
          lastName,
          email,
        },
      }).query();


      if (trainer) {
        const {
          id,
        } = await trainer;

        const token = await userAuthenticate(id);

        const responseHandler = new ResponseHandler(req, res, token);

        return responseHandler.handleResponse({
          token,
        });
      }

      return res.status(404).send({
        message: 'Can\t find trainer',
      });
    }
  }

  return res.status(403).send({
    message: 'Wrong request body',
  });
}

async function getTrainerData(req, res) {
  const {
    params,
  } = req;

  const {
    id,
  } = params;


  if (id) {
    const queryHandler = new QueryHandler({
      model: 'trainer',
      queryType: 'findOne',
      whereQuery: {
        id,
      },
      include: [{
        model: models.access,
        as: 'accessLevel',
        attributes: ['name', 'level'],
      }],
    });

    const trainer = await queryHandler.query();

    if (trainer) {
      const responseHandler = new ResponseHandler(req, res, trainer);

      return responseHandler.handleResponse();
    }
  }
}

module.exports = {
  registerTrainer,
  loginTrainer,
  getTrainerData,
};
