const Sequelize = require('sequelize');

const {
  Op
} = Sequelize;

// Includes
const {
  food_product,
  food_product_details,
  food_category,
  food_category_translate,
  languages,
} = require('../models');

const {
  yandex
} = require('../env');

/**
 * Add new food product
 */
async function addNewFoodProduct(req, res) {
  const {
    body,
    headers
  } = req;

  if (body && headers) {
    const {
      name,
      description,
      weight,
      kcal,
      protein,
      carbs,
      simpleSugars,
      roughage,
      salt,
      fat,
      categoryId,
      languageId,
    } = body;

    const elementExist = await food_product_details
      .findOne({
        where: {
          name: {
            [Op.like]: `%${name}%`,
          },
        },
      })
      .then(exist => exist)
      .catch(noExist => noExist);

    if (elementExist) {
      res.status(403).send('Element already exist');
    } else {
      const addNewProduct = await food_product
        .create({
          name,
          description,
          weight,
          kcal,
          protein,
          carbs,
          simpleSugars,
          roughage,
          salt,
          fat,
          categoryId,
        })
        .then(createdFoodProduct => createdFoodProduct)
        .catch(createdFoodProductError => createdFoodProductError);

      const addNewProductDetails = await food_product_details
        .create({
          name,
          description,
          foodProductId: addNewProduct.id,
          languageId,
        })
        .then(createdProductDetails => createdProductDetails)
        .catch(createdProductDetailsError => createdProductDetailsError);

      res.status(200).send(addNewProduct);
    }
  }
}

/**
 * Get all products
 * @param {*} req
 * @param {*} res
 */
async function getAllProducts(req, res) {
  const allProductsPromise = await food_product_details
    .findAll({
      include: [{
          model: food_product,
          as: 'foodProduct',
          include: [{
            model: food_category,
            as: 'foodCategory',
          }, ],
        },
        {
          model: languages,
          as: 'language',
        },
      ],
    })
    .then(response => response)
    .catch(err => err);

  res.status(200).send(allProductsPromise);
}
module.exports = {
  addNewFoodProduct,
  getAllProducts,
};
