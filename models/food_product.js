module.exports = (sequelize, DataTypes) => {
  const food_product = sequelize.define('food_products', {
    originalName: DataTypes.STRING,
    weight: DataTypes.INTEGER,
    kcal: DataTypes.INTEGER,
    protein: DataTypes.INTEGER,
    carbs: DataTypes.INTEGER,
    simpleSugars: DataTypes.INTEGER,
    roughage: DataTypes.INTEGER,
    salt: DataTypes.INTEGER,
    fat: DataTypes.INTEGER,
    activated: DataTypes.BOOLEAN,
  }, {});
  food_product.associate = (models) => {
    // associations can be defined here
    models.food_products.belongsTo(models.food_categories, {
      as: 'foodCategory',
    });
  };
  return food_product;
};
