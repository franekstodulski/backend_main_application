module.exports = (sequelize, DataTypes) => {
  const food_category = sequelize.define('food_categories', {
    activated: DataTypes.BOOLEAN,
  }, {});
  food_category.associate = (models) => {
    // associations can be defined here
    models.food_categories.hasMany(models.food_category_translates);
  };
  return food_category;
};
