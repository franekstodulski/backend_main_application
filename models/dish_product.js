module.exports = (sequelize, DataTypes) => {
  const dish_product = sequelize.define('dish_product', {
    weight: DataTypes.INTEGER,
    kcal: DataTypes.INTEGER,
    protein: DataTypes.INTEGER,
    carbs: DataTypes.INTEGER,
    simpleSugars: DataTypes.INTEGER,
    roughage: DataTypes.INTEGER,
    salt: DataTypes.INTEGER,
    fat: DataTypes.INTEGER,
  }, {});
  dish_product.associate = (models) => {
    // associations can be defined here
    models.dish_product.belongsTo(models.dish);
  };
  return dish_product;
};
