module.exports = (sequelize, DataTypes) => {
  const access = sequelize.define('access', {
    level: DataTypes.INTEGER,
    name: DataTypes.STRING,
  }, {});
  access.associate = (models) => {
    // associations can be defined here
  };
  return access;
};
