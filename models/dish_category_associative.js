module.exports = (sequelize, DataTypes) => {
  const dish_category_associative = sequelize.define('dish_category_associative', {
    dishID: DataTypes.INTEGER,
    dishCategoryID: DataTypes.INTEGER,
  }, {});
  dish_category_associative.associate = (models) => {
    // associations can be defined here


  };
  return dish_category_associative;
};
