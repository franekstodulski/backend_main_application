module.exports = (sequelize, DataTypes) => {
  const food_product_reported = sequelize.define('food_product_reported', {}, {});
  food_product_reported.associate = (models) => {
    // associations can be defined here
    models.food_product_reported.belongsTo(models.food_products);
  };
  return food_product_reported;
};
