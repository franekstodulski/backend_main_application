module.exports = (sequelize, DataTypes) => {
  const dish_category = sequelize.define('dish_category', {
    name: DataTypes.STRING,
    activated: DataTypes.BOOLEAN,
  }, {});
  dish_category.associate = (models) => {
    // associations can be defined here
  };
  return dish_category;
};
