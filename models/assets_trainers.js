module.exports = (sequelize, DataTypes) => {
  const assets_trainers = sequelize.define('assets_trainers', {
    filePath: DataTypes.STRING,
  }, {});
  assets_trainers.associate = (models) => {
    // associations can be defined here
    models.assets_trainers.belongsTo(models.assets_types, {
      as: 'type',
    });

    models.assets_trainers.belongsTo(models.trainer, {
      as: 'trainer',
    });
  };
  return assets_trainers;
};
