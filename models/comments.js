module.exports = (sequelize, DataTypes) => {
  const comments = sequelize.define('comments', {
    content: DataTypes.STRING,
  }, {});
  comments.associate = (models) => {
    // associations can be defined here
    models.comments.belongsTo(models.dish);
    models.comments.belongsTo(models.trainer);
  };
  return comments;
};
