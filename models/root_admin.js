module.exports = (sequelize, DataTypes) => {
  const root_admin = sequelize.define('root_admins', {
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
  }, {});
  root_admin.associate = (models) => {
    // associations can be defined here
  };
  return root_admin;
};
