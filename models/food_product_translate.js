module.exports = (sequelize, DataTypes) => {
  const food_product_translate = sequelize.define('food_product_translate', {
    name: DataTypes.STRING,
    describtion: DataTypes.STRING,
  }, {});
  food_product_translate.associate = (models) => {
    // associations can be defined here
    models.food_product_translate.belongsTo(models.food_products, {
      as: 'foodProduct',
    });
    models.food_product_translate.belongsTo(models.languages, {
      as: 'language',
    });
  };
  return food_product_translate;
};
