module.exports = (sequelize, DataTypes) => {
  const trainer = sequelize.define('trainer', {
    email: DataTypes.STRING,
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    birthday: DataTypes.STRING,
    location: DataTypes.STRING,
    activated: DataTypes.BOOLEAN,
    recurring_payments: DataTypes.BOOLEAN,
    referer_url: DataTypes.STRING,
    points: DataTypes.INTEGER,
  }, {});
  trainer.associate = (models) => {
    // associations can be defined here
    models.trainer.belongsTo(models.access, {
      as: 'accessLevel',
    });
    models.trainer.hasMany(models.student);
    models.trainer.hasMany(models.assets_trainers);
    models.trainer.hasMany(models.dish);
  };
  return trainer;
};
