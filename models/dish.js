module.exports = (sequelize, DataTypes) => {
  const dish = sequelize.define('dish', {
    name: DataTypes.STRING,
    describtion: DataTypes.STRING,
    likes: DataTypes.INTEGER,
    dislikes: DataTypes.INTEGER,
    activated: DataTypes.BOOLEAN,
  }, {});
  dish.associate = (models) => {
    // associations can be defined here
    models.dish.belongsTo(models.trainer);
  };
  return dish;
};
