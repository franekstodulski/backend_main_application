module.exports = (sequelize, DataTypes) => {
  const food_category_translate = sequelize.define('food_category_translates', {
    name: DataTypes.STRING,
  }, {});
  food_category_translate.associate = (models) => {
    // associations can be defined here
    models.food_category_translates.belongsTo(models.languages, {
      as: 'language',
    });
    models.food_category_translates.belongsTo(models.food_categories, {
      as: 'foodCategory',
    });
  };
  return food_category_translate;
};
