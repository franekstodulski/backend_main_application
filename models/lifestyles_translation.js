module.exports = (sequelize, DataTypes) => {
  const lifestyles_translation = sequelize.define('lifestyles_translation', {
    name: DataTypes.STRING,
  }, {});
  lifestyles_translation.associate = (models) => {
    // associations can be defined here
    models.lifestyles_translation.belongsTo(models.lifestyles, {
      as: 'typeId',
    });

    models.lifestyles_translation.belongsTo(models.languages, {
      as: 'languageId',
    });
  };
  return lifestyles_translation;
};
