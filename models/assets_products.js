module.exports = (sequelize, DataTypes) => {
  const assets_products = sequelize.define('assets_products', {
    filePath: DataTypes.STRING,
  }, {});
  assets_products.associate = (models) => {
    // associations can be defined here
    models.assets_products.belongsTo(models.assets_types, {
      as: 'type',
    });

    models.assets_products.belongsTo(models.food_products, {
      as: 'foodProduct',
    });
  };
  return assets_products;
};
