module.exports = (sequelize, DataTypes) => {
  const student = sequelize.define(
    'student', {
      firstName: DataTypes.STRING,
      lastName: DataTypes.STRING,
      email: DataTypes.STRING,
      birthday: DataTypes.STRING,
      location: DataTypes.STRING,
      weight: DataTypes.INTEGER,
      height: DataTypes.INTEGER,
      waist: DataTypes.INTEGER,
      shoulders: DataTypes.INTEGER,
      chest: DataTypes.INTEGER,
    }, {},
  );
  student.associate = (models) => {
    // associations can be defined here
    models.student.belongsTo(models.trainer, {
      as: 'trainerId',
    });
  };
  return student;
};
