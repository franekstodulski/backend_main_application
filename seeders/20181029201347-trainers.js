module.exports = {
  up: async (queryInterface, Sequelize) => {
    const trainers = [{
      email: 'franek.stodulski@gmail.com',
      firstName: 'Franek',
      lastName: 'Stodulski',
      birthday: '11/11/1996',
      location: 'Wrocław',
      activated: false,
      recurring_payments: true,
      referer_url: 'https://www.fitapp.io/referer_trainer/',
      points: 0,
      createdAt: new Date(),
      updatedAt: new Date(),
      accessLevelId: 1,
    },
    {
      email: 'kamieniu987@gmail.com',
      firstName: 'Franek',
      lastName: 'Stodulski',
      birthday: '11/11/1996',
      location: 'Wrocław',
      activated: false,
      recurring_payments: true,
      referer_url: 'https://www.fitapp.io/referer_trainer/',
      points: 0,
      createdAt: new Date(),
      updatedAt: new Date(),
      accessLevelId: 1,
    },
    ];

    return queryInterface.bulkInsert('trainers', trainers, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  },
};
