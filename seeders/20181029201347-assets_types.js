module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert('assets_types', [{
    type: 'avatar',
    createdAt: new Date(),
    updatedAt: new Date(),
  }, {
    type: 'background',
    createdAt: new Date(),
    updatedAt: new Date(),
  }, {
    type: 'product',
    createdAt: new Date(),
    updatedAt: new Date(),
  }, {
    type: 'category',
    createdAt: new Date(),
    updatedAt: new Date(),
  }, {
    type: 'exercise',
    createdAt: new Date(),
    updatedAt: new Date(),
  }], {}),

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  },
};
