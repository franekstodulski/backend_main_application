const accessArray = [{
  level: 1,
  name: 'low',
  createdAt: new Date(),
  updatedAt: new Date(),
}, {
  level: 2,
  name: 'medium',
  createdAt: new Date(),
  updatedAt: new Date(),
}, {
  level: 3,
  name: 'high',
  createdAt: new Date(),
  updatedAt: new Date(),
}];

const accessTable = accessArray.map(access => ({
  ...access,
}));


module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.bulkInsert('accesses', accessTable, {}),
  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('root_admins', null, {}),
};
