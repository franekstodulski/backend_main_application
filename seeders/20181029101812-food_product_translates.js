const axios = require('axios');

const models = require('../models');

const {
  yandex,
} = require('../env');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const getAllProducts = await queryInterface.sequelize.query('SELECT * FROM food_products', {
      model: models.food_products,
    });

    const insertTranslations = await getAllProducts.map(async (product) => {
      const {
        id,
        originalName,
      } = product;

      // // Translate name
      // const translatedName = await axios({
      //     method: 'get',
      //     url: `https://translate.yandex.net/api/v1.5/tr.json/translate?key=${yandex.translate_api_key}&text=${encodeURI(originalName)}&lang=en`,
      //     headers: {
      //       'Content-Type': 'application/x-www-form-urlencoded',
      //     },
      //   })
      //   .then(translateRes => (translateRes.data ? translateRes.data.text[0] : ''))
      //   .catch(error => console.log(error));


      return {
        name: originalName,
        foodProductId: id,
        languageId: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      };
    });

    const translatedNames = await Promise.all(await insertTranslations);

    return queryInterface.bulkInsert('food_product_translates', translatedNames, {});
  },


  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('root_admins', null, {}),
};
