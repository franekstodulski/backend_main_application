module.exports = {
  up: async (queryInterface, Sequelize) => {
    const students = [{
      email: 'franek.stodulski@gmail.com',
      firstName: 'Franek',
      lastName: 'Stodulski',
      birthday: '11/11/1996',
      location: 'Wrocław',
      weight: 97,
      height: 195,
      waist: 99,
      shoulders: 167,
      chest: 105,
      createdAt: new Date(),
      updatedAt: new Date(),
      trainerId: 1,
    },
    {
      email: 'kamieniu987@gmail.com',
      firstName: 'Franek',
      lastName: 'Stodulski',
      birthday: '11/11/1996',
      location: 'Wrocław',
      weight: 97,
      height: 195,
      waist: 99,
      shoulders: 167,
      chest: 105,
      createdAt: new Date(),
      updatedAt: new Date(),
      trainerId: 2,
    },
    ];

    return queryInterface.bulkInsert('students', students, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  },
};
