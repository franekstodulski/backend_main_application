const glob = require('glob');

/**
 * Return list of file paths for mock files
 */
async function getAllFiles() {
  const filesPaths = await glob.sync('mocks/static/products/**.json');
  return filesPaths;
}


module.exports = {
  up: async (queryInterface, Sequelize) => {
    const filesPathsList = await getAllFiles();

    const mapFilesPathsList = filesPathsList.map(async (filePath) => {
      const singleFile = require(`../${filePath}`);

      // Destruct object for variables
      const {
        title,
      } = singleFile;

      const {
        products,
      } = singleFile.data;

      /**
       * Return id of existing category mateched by name of category
       */
      const existsCategoryID = await queryInterface.rawSelect('food_category_translates', {
        where: {
          name: title,
        },
      }, ['id']);


      if (existsCategoryID) {
        const mapEachProduct = await products.map(async (product) => {
          const {
            productName,
            macros,
            weight,
          } = product;

          /**
           * Translate macros
           */
          const macrosArray = macros.reduce((map, obj) => {
            switch (obj.label) {
              case 'Białko':
                obj.label = 'protein';
                break;
              case 'Tłuszcz':
                obj.label = 'fat';
                break;

              case 'Energia':
                obj.label = 'kcal';
                break;

              case 'Węglowodany':
                obj.label = 'carbs';
                break;

              case 'Cukry proste':
                obj.label = 'simpleSugars';
                break;

              case 'Błonnik':
                obj.label = 'roughage';
                break;
              case 'Sól':
                obj.label = 'salt';
                break;
              default:
            }

            const newObj = {
              [obj.label]: parseInt([obj.value], 10) || 0,
            };

            return {
              ...map,
              ...newObj,
            };
          }, {});

          return {
            originalName: productName,
            foodCategoryId: existsCategoryID,
            weight: parseFloat(weight, 10),
            ...macrosArray,
            activated: true,
            createdAt: new Date(),
            updatedAt: new Date(),
          };
        });


        const allProductsInCategory = await Promise.all(await mapEachProduct);

        return allProductsInCategory;
      }
    });

    const productsArrays = await Promise.all(await mapFilesPathsList);

    const bultInsertPromise = productsArrays.map(productsArray => queryInterface.bulkInsert('food_products', productsArray, {}));

    return Promise.all(bultInsertPromise);
  },

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('root_admins', null, {}),
};
