const axios = require('axios');

const models = require('../models');

const {
  yandex,
} = require('../env');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const categories = await queryInterface.sequelize.query('SELECT * FROM food_category_translates', {
      model: models.food_category_translates,
    });


    const translateCategories = await categories.map(async (category) => {
      const {
        id,
        name,
        foodCategoryId,
      } = category;

      const translatedName = await axios({
        method: 'get',
        url: `https://translate.yandex.net/api/v1.5/tr.json/translate?key=${yandex.translate_api_key}&text=${encodeURI(name)}&lang=en`,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
        .then(translateRes => (translateRes.data ? translateRes.data.text[0] : ''))
        .catch(error => console.log(error));

      return {
        name: translatedName,
        foodCategoryId,
        languageId: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      };
    });

    const translatedCategories = await Promise.all(await translateCategories);

    // console.log(translatedCategories)
    return queryInterface.bulkInsert('food_category_translates', translatedCategories, {});
    // return Promise.all(categoriesNames)
  },

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('root_admins', null, {}),
};
