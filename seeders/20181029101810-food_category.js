const categories = require('../mocks/static/categories/categories.json');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const categoriesNames = await categories.map(async (category) => {
      const foodCategoryEntity = await queryInterface.bulkInsert('food_categories', [{
        activated: true,
        createdAt: new Date(),
        updatedAt: new Date(),
      }]);

      const foodCategoryTranslateEntity = await queryInterface.bulkInsert('food_category_translates', [{
        name: category.title,
        languageId: 1,
        foodCategoryId: foodCategoryEntity,
        createdAt: new Date(),
        updatedAt: new Date(),
      }]);
    });

    return Promise.all(categoriesNames);
  },

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('root_admins', null, {}),
};
