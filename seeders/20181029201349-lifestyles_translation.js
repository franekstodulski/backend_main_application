module.exports = {
  up: async (queryInterface, Sequelize) => {
    const lifeStylesTranslations = [{
      name: 'siedzący',
      typeId: 1,
      languageId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      name: 'sitting',
      typeId: 1,
      languageId: 2,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      name: 'stojący',
      typeId: 2,
      languageId: 1,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      name: 'standing',
      typeId: 2,
      languageId: 2,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    ];

    return queryInterface.bulkInsert('lifestyles_translations', lifeStylesTranslations, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
  },
};
