const languages = require('../mocks/database/languages/languages.json');


const languagesArray = languages.map(language => ({
  ...language,
  createdAt: new Date(),
  updatedAt: new Date(),
}));

module.exports = {
  up: async (queryInterface, Sequelize) => queryInterface.bulkInsert('languages', languagesArray, {}),

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('languages', null, {}),
};
